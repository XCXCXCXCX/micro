package org.micro.service.um;

import org.micro.service.um.entity.UserEntity;
import org.micro.service.um.entity.dto.UserDTO;
import org.micro.service.um.func.UserFunc;

public class UserFuncTest {

    public static void main(String[] args) {
        UserEntity userEntity = new UserEntity();
        userEntity.setAge(12);
        userEntity.setName("张三");
        UserDTO userDTO = UserFunc.INSTANCE.toFunc12(userEntity);
        System.out.println(userEntity.toString());
        System.out.println(userDTO.toString());
    }

}
