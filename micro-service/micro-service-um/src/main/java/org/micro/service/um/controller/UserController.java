package org.micro.service.um.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.extern.slf4j.Slf4j;
import org.micro.service.um.entity.vo.UserInfoVO;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@Slf4j
@RestController
public class UserController {

    @HystrixCommand(fallbackMethod = "fallbackMethodHetUserInfoVO")
    @RequestMapping("user-info")
    public UserInfoVO getUserInfoVO() {
        log.debug("This is debug.");
        log.info("This is info.");
        log.warn("This is warn.");
        log.trace("This is trace.");
        log.error("This is error.");
        return new UserInfoVO();
    }

    @RequestMapping("time")
    public String time() {
        int d = 1 / 0;
        return new Date().toString();
    }

    public UserInfoVO fallbackMethodHetUserInfoVO() {
        return new UserInfoVO();
    }

}
