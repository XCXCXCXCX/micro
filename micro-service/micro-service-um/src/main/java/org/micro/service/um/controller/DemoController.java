package org.micro.service.um.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;

@Slf4j
@Controller
public class DemoController {

    @RequestMapping("demo")
    public String demo() {
        return new Date().toString();
    }

}
