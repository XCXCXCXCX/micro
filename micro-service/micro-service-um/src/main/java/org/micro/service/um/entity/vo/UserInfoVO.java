package org.micro.service.um.entity.vo;

import lombok.*;
import org.micro.service.um.entity.UserEntity;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The UserEntity Info
 *
 * @author lry
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class UserInfoVO implements Serializable {

    private String id;
    private String email;
    private String tel;
    private String idCard;

    private String name;
    private Integer age;

}
