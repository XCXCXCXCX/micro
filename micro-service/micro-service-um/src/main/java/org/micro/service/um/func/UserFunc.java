package org.micro.service.um.func;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.micro.frame.commons.func.Func;
import org.micro.service.um.entity.UserEntity;
import org.micro.service.um.entity.dto.UserDTO;
import org.micro.service.um.entity.vo.UserInfoVO;

/**
 * 模型转换器
 *
 * @author lry
 */
@Mapper
public interface UserFunc extends Func.Func3<UserEntity, UserDTO, UserInfoVO> {

    UserFunc INSTANCE = Mappers.getMapper(UserFunc.class);

}
