package org.micro.service.um.service.impl;

import org.micro.service.um.entity.UserEntity;
import org.micro.service.um.entity.dto.UserDTO;
import org.micro.service.um.func.UserFunc;
import org.micro.service.um.service.IUserService;
import org.springframework.stereotype.Service;

/**
 * The UserEntity Service Implements
 *
 * @author lry
 */
@Service
public class UserServiceImpl implements IUserService {

    private UserFunc userFunc = UserFunc.INSTANCE;

    public void test() {
        // eg: UserEntity -> UserDAO
        UserEntity userEntity = new UserEntity();
        UserDTO userDTO = userFunc.toFunc12(userEntity);
    }

}
