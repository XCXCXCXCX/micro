package org.micro.service.um.entity;

import lombok.*;
import org.micro.frame.commons.entity.BaseEntity;

/**
 * The UserEntity Entity
 *
 * @author lry
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class UserEntity extends BaseEntity {

    private String email;
    private String tel;
    private String idCard;

    private String name;
    private Integer age;

}
