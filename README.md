# 微服务(Micro)

[TOC]

**前言**：
基于Spring Boot + Spring Cloud 体系，打造一套微服务框架,开箱即用的效果!

## 1 注册中心(micro-eureka)


## 2 应用管理(micro-admin)


## 3 链路追踪(micro-zipkin)


## 4 微框架(micro-frame)


## 5 微服务(micro-service)


