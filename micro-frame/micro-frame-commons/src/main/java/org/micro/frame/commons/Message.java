package org.micro.frame.commons;

import lombok.*;

import java.io.Serializable;

/**
 * The Message
 *
 * @author lry
 */
@Data
@ToString
@EqualsAndHashCode
public class Message implements Serializable {

    private boolean success;
    private int code;
    private String message;
    private Object data;

    private Message(boolean success, int code, String message, Object data) {
        this.success = success;
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static Message buildMessage(boolean success, int code, String message, Object data) {
        return new Message(success, code, message, data);
    }

}
