package org.micro.frame.commons.entity;

import lombok.*;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * The Base Entity
 *
 * @author lry
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class BaseEntity implements Serializable {

    /**
     * 注解ID
     */
    protected Long id;
    /**
     * 创建者
     */
    protected Long creator;
    /**
     * 创建时间
     */
    protected Timestamp createTime;
    /**
     * 操作者
     */
    protected Long operator;
    /**
     * 操作时间
     */
    protected Timestamp operateTime;
    /**
     * 逻辑删除
     */
    protected Integer deleteFlag;

}
