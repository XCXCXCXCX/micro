package org.micro.frame.commons.func;

import java.io.Serializable;

/**
 * The Convert by Func
 *
 * @param <F1> Func1
 * @param <F2> Func2
 * @author lry
 */
public interface Func<F1 extends Serializable, F2 extends Serializable> {

    /**
     * The Func2 to convert Func1
     *
     * @param f2 Func2
     * @return Func1
     */
    F1 toFunc21(F2 f2);

    /**
     * The Func1 to convert Func2
     *
     * @param f1 Func1
     * @return Func2
     */
    F2 toFunc12(F1 f1);

    /**
     * The Convert by F1,F2
     *
     * @param <F1> Func1
     * @param <F2> Func2
     * @author lry
     */
    interface Func2<F1 extends Serializable, F2 extends Serializable> extends Func<F1, F2> {

    }

    /**
     * The Convert by F1,F2,F3
     *
     * @param <F1> Func1
     * @param <F2> Func2
     * @param <F3> Func3
     * @author lry
     */
    interface Func3<F1 extends Serializable, F2 extends Serializable, F3 extends Serializable> extends Func2<F1, F2> {

        /**
         * The Func1 to convert Func3
         *
         * @param f1 Func1
         * @return Func3
         */
        F3 toFunc13(F1 f1);

        /**
         * The Func2 to convert Func3
         *
         * @param f2 Func2
         * @return Func3
         */
        F3 toFunc23(F2 f2);

        /**
         * The Func3 to convert Func1
         *
         * @param f3 Func3
         * @return Func1
         */
        F1 toFunc31(F3 f3);

        /**
         * The Func3 to convert Func2
         *
         * @param f3 Func3
         * @return Func2
         */
        F2 toFunc32(F3 f3);

    }

    /**
     * The Convert by F1,F2,F3,F4
     *
     * @param <F1> Func1
     * @param <F2> Func2
     * @param <F3> Func3
     * @param <F4> Func4
     * @author lry
     */
    interface Func4<F1 extends Serializable, F2 extends Serializable,
            F3 extends Serializable, F4 extends Serializable> extends Func3<F1, F2, F3> {

        /**
         * The Func1 to convert Func4
         *
         * @param f1 Func1
         * @return Func4
         */
        F4 toFunc14(F1 f1);

        /**
         * The Func2 to convert Func4
         *
         * @param f2 Func2
         * @return Func4
         */
        F4 toFunc24(F2 f2);

        /**
         * The Func3 to convert Func4
         *
         * @param f3 Func3
         * @return Func4
         */
        F4 toFunc34(F3 f3);


        /**
         * The Func4 to convert Func1
         *
         * @param f4 Func4
         * @return Func1
         */
        F1 toFunc41(F4 f4);

        /**
         * The Func4 to convert Func3
         *
         * @param f4 Func4
         * @return Func2
         */
        F2 toFunc42(F4 f4);

        /**
         * The Func4 to convert Func3
         *
         * @param f4 Func4
         * @return Func3
         */
        F3 toFunc43(F4 f4);

    }

    /**
     * The Convert by F1,F2,F3,F4
     *
     * @param <F1> Func1
     * @param <F2> Func2
     * @param <F3> Func3
     * @param <F4> Func4
     * @param <F5> Func5
     * @author lry
     */
    interface Func5<F1 extends Serializable, F2 extends Serializable, F3 extends Serializable,
            F4 extends Serializable, F5 extends Serializable> extends Func4<F1, F2, F3, F4> {

        /**
         * The Func1 to convert Func5
         *
         * @param f1 Func1
         * @return Func5
         */
        F5 toFunc15(F1 f1);

        /**
         * The Func2 to convert Func5
         *
         * @param f2 Func2
         * @return Func5
         */
        F5 toFunc25(F2 f2);

        /**
         * The Func3 to convert Func5
         *
         * @param f3 Func3
         * @return Func5
         */
        F5 toFunc35(F3 f3);

        /**
         * The Func4 to convert Func5
         *
         * @param f4 Func4
         * @return Func5
         */
        F5 toFunc45(F4 f4);

        /**
         * The Func5 to convert Func1
         *
         * @param f5 Func5
         * @return Func1
         */
        F1 toFunc51(F5 f5);

        /**
         * The Func5 to convert Func2
         *
         * @param f5 Func5
         * @return Func2
         */
        F2 toFunc52(F5 f5);

        /**
         * The Func5 to convert Func3
         *
         * @param f5 Func5
         * @return Func3
         */
        F3 toFunc53(F5 f5);

        /**
         * The Func5 to convert Func4
         *
         * @param f5 Func5
         * @return Func4
         */
        F4 toFunc54(F5 f5);

    }

}