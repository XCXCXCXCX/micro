package org.micro.frame.service;

/**
 * The Constants
 *
 * @author lry
 */
public class Constants {
    public static final String CHARACTER_ENCODING = "UTF-8";
    public static final String TRACE_ID = "Trace-Id";
    public static final String REQUEST_ID = "Request-Id";
}
