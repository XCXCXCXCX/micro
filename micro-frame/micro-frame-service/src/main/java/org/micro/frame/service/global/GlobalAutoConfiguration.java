package org.micro.frame.service.global;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.autoconfigure.web.ErrorMvcAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The Global Exception Auto Configuration
 *
 * @author lry
 */
@Slf4j
@Configuration
@ConditionalOnWebApplication
@AutoConfigureBefore(ErrorMvcAutoConfiguration.class)
public class GlobalAutoConfiguration {

    @Bean
    public GlobalControllerHandler globalHandler() {
        log.info("===>> Configure GlobalControllerHandler.");
        return new GlobalControllerHandler();
    }

}
