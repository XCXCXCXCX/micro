package org.micro.frame.service.adapter;

import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.micro.frame.service.interceptor.MicroHandlerInterceptor;
import org.micro.frame.service.interceptor.MicroOncePerRequestFilter;
import org.micro.frame.service.jackson.JacksonSerializerFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.annotation.Resource;

/**
 * 该配置类覆盖了原有的HttpMessageConverters处理方式，采用fastjson处理方式
 *
 * @author lry
 */
@Configuration
public class WebConfigure extends WebMvcConfigurerAdapter {

    @Resource
    private ObjectMapper objectMapper;

    @Bean
    public ObjectMapper jsonMapper() {
        JacksonSerializerFactory.wrapperSerializerFactory(objectMapper);
        return objectMapper;
    }

    @Bean
    public HttpMessageConverters useConverters() {
        return new HttpMessageConverters(new FastJsonHttpMessageConverter());
    }

    @Bean
    public MicroOncePerRequestFilter microOncePerRequestFilter() {
        return new MicroOncePerRequestFilter();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new MicroHandlerInterceptor()).addPathPatterns("/**");
    }

}